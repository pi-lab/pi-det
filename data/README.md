* 需要对`coco.yaml`文件进行更改，将其中的文件路径进行替换，并将类别数和名称进行修改，例如： 

```
train: ../your_path/train.txt  
val: ../your_path/val.txt  
test: ../your_path/test.txt  

# number of classes
nc: 3

# class names
names: ['person', 'bicycle', 'car']
```

* 将类别名称写入文件`data.names` ，例如：

```
person
bicycle
car
```
