## InstallConda

可通过清华镜像下载https://mirrors.tuna.tsinghua.edu.cn/anaconda/archive/，然后进入安装目录，执行以下命令，并一路点enter和选择yes

```
bash Anaconda3-4.4.0-Linux-x86_64.sh
```

如果不想安装VSCode，可在`Do you wish to proceed with the installation of Microsoft VSCode?[yes|no]`后选择no

### Conda使用cuda

```
conda install cudatoolkit=8.0 -c https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/linux-64/
```

### Conda创建自己的环境
```
conda create -n my_pytorch
```

### Conda激活环境
```
conda activate my_pytorch
```

### Conda常用命令
```
# 帮助命令
conda -h
conda help

# 配置频道(已有)
conda config --add channels https://mirrors.bfsu.edu.cn/anaconda/pkgs/main/

# 退出当前环境
conda deactivate

# 查看基本信息
conda info
conda info -h

# 查看当前存在环境
conda env list
conda info --envs

# 删除环境
conda remove -n yourname --all
```
