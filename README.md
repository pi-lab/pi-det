##   PI-Det

### 1.简介

`PI-Det`是面向无人机航拍图像的物体检测，基于`PyTorch_YOLOv4`的基础上进行改进，并结合增量学习方法，可通过使用新类别物体的数据进行训练，并保持一定的对旧类别物体的检测能力，避免神经网络发生灾难性遗忘，技术文档见[无人机航拍图像物体检测](./doc/technology.pdf)

<img src="./picture/cover.jpg" width="800" />

### 2.安装依赖

可通过下面这一行命令安装依赖（建议使用Ubuntu18.04）

```
pip install -r requirements.txt
```

并安装mish-cuda

```
git clone https://github.com/thomasbrandon/mish-cuda
cd mish-cuda
cp external/CUDAApplyUtils.cuh csrc        # cuda >= 11.0
python setup.py build install
```

本程序最好在Conda环境下运行，Conda的运行环境安装见[InstallConda](./doc/InstallConda.md)

### 3.数据准备

训练前需要将图像和标注分别放在` images`和`labels`文件夹下，并将训练集所有图像路径写入`train.txt`文本中，验证集所有图像路径写入`val.txt`文本中，测试集所有图像路径写入`test.txt`文本中，然后对`coco.yaml`文件进行更改，将其中的文件路径进行替换，例如：

```
train: ../your_path/train.txt  
val: ../your_path/val.txt  
test: ../your_path/test.txt  

# number of classes
nc: 3

# class names
names: ['person', 'bicycle', 'car']
```

数据标注格式与YOLOv4一致，即`<object-class> <x_center> <y_center> <width> <height>`，分别代表物体类别，中心点坐标(x,y)，边界框的宽和高(w,h)，`<x_center> <y_center> <width> <height>`均为归一化后的坐标，例如：

```
2 0.44091796875 0.1953125 0.2685546875 0.021484375
1 0.55029296875 0.173828125 0.0517578125 0.0234375
1 0.5517578125 0.5478515625 0.046875 0.02734375
```

示例数据集和预训练权重可在百度网盘下载：

链接: https://pan.baidu.com/s/1YZIckwLbVOVpSMuyt3ljwg 密码: inl0

### 4.训练

训练前需要对[cfg](./cfg/yolov4-new.cfg)文件进行修改，将其中每个`[yolo]`层中的`classes` 修改为所使用的类别数，并将前一个卷积层的`filters` 改为`(classes+5)x3` ，例如：

```
[convolutional]
filters=255

[yolo]
classes=80
```

如果使用多GPU进行训练，可将`--device 0`改为`--device 0,1,2,3`；如果不想每训练完一个epoch就进行验证，可在命令中加上`--notest`

```
python train.py --device 0 --batch-size 16 --img 512 512 --data data/your_yaml --cfg cfg/your_cfg --weights weights/pretrained_weight --name your_name --epochs 100
```

如果要通过增量学习新类别物体，可使用以下命令运行`train_incremental.py` 

```
python train_incremental.py --device 0 --batch-size 16 --img 512 512 --data data/your_yaml --cfg cfg/new_model_cfg --weights weights/your_weights --name your_name --epochs 100 --notest --incremental --old_mod cfg/old_model_cfg
```

### 5.测试

```
python test.py --img 512 --conf 0.001 --batch 16 --device 0 --data data/your_yaml --cfg cfg/your_cfg --weights/your_weights  --name data/data.names
```

可使用上面的命令进行测试，需要在`--name` 后输入含有类别名称的文件`data.names` ，例如：

```
person
bicycle
car
```

### 6.检测

可使用以下命令对图像或视频进行检测，需要在`--weights`后输入训练好的权重文件，在`--source`输入需要检测的图像或视频，检测结果保存在`/inference/output/`下

```
python detect.py --img-size 512 --weights weights/your_weights --device 0 --cfg cfg/yolov4-new.cfg --names data/dota.names --source detect_image  or  video
```

部分检测结果如下：

<img src="./picture/result_1.jpg" width="700" />

<img src="./picture/result_2.jpg" width="700" />

### 参考

* YOLOv4：https://arxiv.org/abs/2004.10934
* Learning without Forgetting：https://arxiv.org/abs/1606.09282

* Class-Balanced Loss Based on Effective Number of Samples：https://arxiv.org/abs/1901.05555
* RILOD：https://arxiv.org/abs/1904.00781
* PyTorch_YOLOv4：https://github.com/WongKinYiu/PyTorch_YOLOv4 （PI-Det基于该代码进行改进）